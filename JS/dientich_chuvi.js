/**
 * Input:
 * - Chiều dài = 5cm
 * - Chiều rộng= 3cm
 *
 *
 * Steps
 * S1: tạo 2 biến lưu 2 input
 * S2: tạo 2 biến lưu output dienTich, chuVi
 * S3: sử dụng công thức tính diện tích và chu vi hình chữ nhật
 *
 * Output
 * - diện tích = 15
 * - chu vi = 16
 *
 */
var chieuDai = 5;
var chieuRong = 3;

var dienTich = null;
dienTich = chieuDai * chieuRong;

var chuVi = null;
chuVi = (chieuDai + chieuRong) * 2;
console.log(dienTich, chuVi);
