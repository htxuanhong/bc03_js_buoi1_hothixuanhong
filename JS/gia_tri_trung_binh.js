/**
 * Input: 3, 2, 8, 6, 5
 * Steps:
 * S1: tạo 5 biến lưu 5 input
 * s2: tạo 1 biến lưu output
 * S3: tính giá trị trung bình bằng tổng 5 số thực chia 5
 *
 * output:4.8
 */

var a = 3;
var b = 2;
var c = 8;
var d = 6;
var e = 5;

var tb = null;
tb = (a + b + c + d + e) / 5;
console.log(tb);
