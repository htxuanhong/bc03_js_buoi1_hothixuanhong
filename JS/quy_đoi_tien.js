/**
 * INPUT:
 * - Tỉ giá: 23.5
 * - Tiền USD: người dùng tự nhập
 * Steps
 * - S1: tạo form trên html để người dùng nhập giá trị USD
 * - S3: tạo biến lưu onclick
 * - S2: tạo 2 biến lưu input
 * - S3: tạo 1 biến lưu output
 * - S4: áp dụng công thức tính
 *
 * OUTPUT: doiDuoc
 */

var doiTien = function () {
  var tienUsd = document.getElementById("txt_tien_doi").value * 1;
  var tiGia = 23.5;

  var doiDuoc = tienUsd * tiGia;
  console.log(doiDuoc);

  document.getElementById("tien_tinh_duoc").innerHTML = `<div>
    <p>Số tiền: ${doiDuoc} 000 vnđ</p>
    </div>`;
};
