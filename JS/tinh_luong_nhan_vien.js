/**
 * INPUT:
 * - Lương 1 ngày: 100
 * - Số ngày công: người dùng tự nhập
 * Steps
 * - S1: tạo form trên html để người dùng nhập số ngày công
 * - S3: tạo biến lưu onclick
 * - S2: tạo 2 biến lưu input
 * - S3: tạo 1 biến lưu output
 * - S4: áp dụng công thức tính
 *
 * OUTPUT: ketQua
 */

var tinhTien = function () {
  var soNgay = document.getElementById("txt_so_ngay").value * 1;
  var luong1ngay = 100;

  var ketQua = soNgay * luong1ngay;
  console.log(ketQua);

  document.getElementById("ket_qua").innerHTML = `<div>
  <p>Tổng tiền lương: ${ketQua} 000 vnđ</p>
  </div>`;
};
