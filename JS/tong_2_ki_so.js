/**
 * Input: 34
 

 * Step
 *S1: tạo 2 biến lưu chuc, donvi
 *S2: tạo biến lưu input
 * S3: tạo biến lưu output
 * S4: tính chuc, donvi
 * S4: lấy tổng chuc, donvi

 * Output: 7
 */

var chuc, donvi;
var input = 34;
var output = null;

donvi = input % 10;
chuc = Math.floor(input / 10);

output = donvi + chuc;

console.log({ output });
